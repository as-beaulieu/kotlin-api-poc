package com.poc.kotlin_api.models

data class SimpleNumberModel(val id: Long, val content: Long)