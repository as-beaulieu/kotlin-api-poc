package com.poc.kotlin_api.models

data class SimpleGreetingModel(val id: Long, val content: String)
