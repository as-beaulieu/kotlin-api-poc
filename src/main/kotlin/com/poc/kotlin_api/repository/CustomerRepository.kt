package com.poc.kotlin_api.repository

import com.poc.kotlin_api.models.CustomerModel
import org.springframework.data.repository.CrudRepository

interface CustomerRepository : CrudRepository<CustomerModel, Long>