package com.poc.kotlin_api.controllers

import com.poc.kotlin_api.models.SimpleNumberModel
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class NumberController {

    val counter = AtomicLong()

    @GetMapping("/api/simple-number")
    fun random(@RequestParam(defaultValue = "100") number: Long) =
            SimpleNumberModel(counter.incrementAndGet(), number)

}