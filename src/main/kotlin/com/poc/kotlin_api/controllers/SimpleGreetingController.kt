package com.poc.kotlin_api.controllers

import com.poc.kotlin_api.models.SimpleGreetingModel
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("/api/simple-greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
            SimpleGreetingModel(counter.incrementAndGet(), "Hello, $name")
}