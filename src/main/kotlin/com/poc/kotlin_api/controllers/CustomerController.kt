package com.poc.kotlin_api.controllers

import com.poc.kotlin_api.models.CustomerModel
import com.poc.kotlin_api.repository.CustomerRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("/api/customers")
class CustomerController(val repository: CustomerRepository) {

    @GetMapping
    fun findAll() = repository.findAll()

    @GetMapping("/{id}")
    fun getCustomerById(@PathVariable id: Long) =
            repository
                    .findById(id)
                    .map{ResponseEntity.ok().body(it)}
                    .orElse(ResponseEntity.notFound().build())

    @PostMapping
    fun addCustomer(@RequestBody customerModel: CustomerModel) =
        Optional
                .of(repository.save(customerModel))
                .map{ResponseEntity.status(201).body(customerModel)}
                .orElse(ResponseEntity.badRequest().build())

    @PutMapping("/{id}")
    fun updateCustomer(
            @PathVariable id: Long,
            @RequestBody customerModel: CustomerModel) =
            repository
                    .findById(id)
                    .map{it.copy(firstName = customerModel.firstName, lastName = customerModel.lastName)}
                    .map{ResponseEntity.ok().body(it)}
                    .orElse(ResponseEntity.notFound().build())

    @DeleteMapping("/{id}")
    fun removeCustomer(@PathVariable id: Long) =
            repository
                .findById(id)
                .map{repository.deleteById(id)}
                .map{ResponseEntity.ok().build<Any>()}
                .orElse(ResponseEntity.notFound().build())

}